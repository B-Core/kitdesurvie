kitdesurvie.directive('alphabetsorting', ['$document',"$timeout", function($document, $timeout) {
  var delay = 0;
  return {
    link: function(scope, element, attr) {
      function animate() {
        element[0].style.transform = "translate(0)";
        element[0].style.webkitTransform = "translate(0)";
      };
      function display() {
        if(element[0].innerHTML !== ""){
          element[0].style.display = "block";
          element[0].style.transitionDelay = delay + "s";
          delay += 0.1;
          $timeout(animate, 200);
        }
      }
      $timeout(display,500);
    }
  };
}]);
