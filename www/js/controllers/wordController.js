kitdesurvie.controller('wordController', ['$scope',"kitdesurvieService", function($scope, kitdesurvieService) {
  var wordController = this;
  wordController.words = [];
  wordController.selectedObjectIndex = null;
  $scope.searchingTerms = [];

  wordController.selectObject = function($index) {
      wordController.selectedObjectIndex = $index;
  }

  var genID = function() {
    var randLetter = String.fromCharCode(65 + Math.floor(Math.random() * 26)),
        combi = randLetter + Date.now();
    return combi
  };

  wordController.addWord = function() {
    toggleOverlay();
    animateWordPopUp(true);
  };

  wordController.sendWord = function() {
    event.preventDefault();
    animateWordPopUp(true);
    var newWord = {
      title:wordController.title,
      description:wordController.description
    }
    if((typeof newWord.title !== "undefined" && newWord.title !== "") && (typeof newWord.description !== "undefined" && newWord.description !== "")) {
      kitdesurvieService.pushData("mots",newWord,
        function(data){
          wordController.title = "";
          wordController.description = "";
          loadRemoteData();
      },function(error){
        console.log(error);
      });
    }
  }

  wordController.removeData = function() {
    kitdesurvieService.removeData("mots",event.target.getAttribute("data-id"),
        function(data){
        loadRemoteData();
      },function(error){
        console.log(error);
      });
  };

  wordController.promptModifyData = function($index) {
    event.preventDefault();
    toggleOverlay();
    wordController.selectObject($index);
    animateWordPopUp(false);
  }

  wordController.modifyData = function() {
    var dataToChange = wordController.words[wordController.selectedObjectIndex];
    kitdesurvieService.modifyData("mots",dataToChange.id,dataToChange,
        function(data){
        loadRemoteData();
      },function(error){
        console.log(error);
      });
  };

  // I apply the remote data to the local scope.
  function applyRemoteData( queriedData ) {
      wordController.words = queriedData;
  }
  // I load the remote data from the server.
  function loadRemoteData() {
      // The friendService returns a promise.
      kitdesurvieService.getData("mots?_sort=title&_order=ASC",
          function(data){
          applyRemoteData( data.data );
        },function(error){
          console.log(error);
        });
  };

  // function addSelectAttribute() {
  //   var wrapper = _(".item-wrapper")[0],
  //       selectedDivs = Array.prototype.slice.call(wrapper.children),
  //       scrollHeight = wrapper.scrollHeight,
  //       scrollTop = wrapper.scrollTop;
  //
  //   wrapper.addEventListener("scroll", function(event){
  //     down = typeof down == "undefined" ? false : down;
  //     if(this.scrollTop >= window.innerHeight && window.innerWidth < 800 && !down) {
  //       _(".searchbar")[0].style.transform = "translate(0,0)";
  //       _(".searchbar")[0].style.webkitTransform = "translate(0,0)";
  //       down = true;
  //     } else if(this.scrollTop < window.innerHeight && window.innerWidth < 800 && down){
  //       _(".searchbar")[0].style.transform = "translate(0,-100px)";
  //       _(".searchbar")[0].style.webkitTransform = "translate(0,-100px)";
  //       down = false;
  //     }
  //     });
  //
  //   wrapper.addEventListener("scroll", function(event){
  //     selectedDivs.forEach(function(item,index){
  //       if(index > 3 && item.type !=="submit" ) {
  //         var boundingRect = item.getBoundingClientRect();
  //         if(window.innerWidth > 800) {
  //           if((scrollTop+scrollHeight/2-boundingRect.height) >= boundingRect.top && (scrollTop+scrollHeight/2-boundingRect.height) <= boundingRect.bottom) {
  //             item.style.backgroundColor = "#c1ede8";
  //             item.style.color = "rgb(100, 102, 103)";
  //           } else {
  //             item.style.backgroundColor = "white";
  //             item.style.color = "black";
  //           }
  //         } else {
  //           if(scrollTop+(window.innerHeight/2) >= boundingRect.top && scrollTop+(window.innerHeight/2) <= boundingRect.bottom) {
  //             item.style.backgroundColor = "#c1ede8";
  //             item.style.color = "rgb(100, 102, 103)";
  //           } else {
  //             item.style.backgroundColor = "white";
  //             item.style.color = "black";
  //           }
  //         }
  //       }
  //     });
  //   })
  //
  // };

  function normalizeString(str) {
    var nStr = "";
    nStr = str.replace(/\s|\[|\]|\{|\}/gim,"");
    nStr = str.replace(/-/gim,"\-");
    nStr = str.replace(/\./gim,"\.");
    nStr = str.replace(/\s/gim,"");
    return nStr;
  }

  loadRemoteData();
  // window.setTimeout(addSelectAttribute, 500);
  toggleResearchBar();
}]);
kitdesurvie.directive('words', function() {
  return {
    templateUrl: 'js/directives/words.html'
  };
});
