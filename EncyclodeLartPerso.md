##Mon Encyclopédie Perso

Présentation : Action, fait de montrer quelque chose ou quelqu'un à un ensemble de personnes, d'exécuter quelque chose devant un public.

Présenter : Mettre quelque chose à la portée de, devant quelqu'un.

Représentation : Action de rendre quelque chose présent à quelqu'un en montrant, en faisant savoir

Reproduction : Le fait de produire une représentation plus ou moins fidèle du réel, d'une réalité ; Le fait d'exécuter une copie, une imitation de quelque chose, à l'aide d'une technique, dans un format, une matière différents de l'original

Expression : Action de rendre manifeste par toutes les possibilités du langage, plus particulièrement par celles du langage parlé et écrit, ce que l'on est, pense ou ressent

Symbolisition : Fait d'utiliser des symboles; action de représenter quelque chose par un symbole ou par des symboles

Symbol : Objet sensible, fait ou élément naturel évoquant, dans un groupe humain donné, par une correspondance analogique, formelle, naturelle ou culturelle, quelque chose d'absent ou d'impossible à percevoir.

Abstraction : Opération intellectuelle, spontanée ou systématique, qui consiste à abstraire 

Abstraire : Isoler, par l'analyse, un ou plusieurs éléments du tout dont ils font partie, de manière à les considérer en eux-mêmes et pour eux-mêmes

Figuration : Fait de donner d'un élément une représentation qui en rende perceptible (surtout à la vue) l'aspect ou la nature caractéristique.

Lumière : Ce par quoi les choses sont éclairées, clarté, lumière

Couleur : Qualité de la lumière que renvoie un objet et qui permet à l'œil de le distinguer des autres objets, indépendamment de sa nature et de sa forme

Matière : Substance dont sont faits les corps perçus par les sens et dont les caractéristiques fondamentales sont l'étendue et la masse

Matériaux : Type de matière qui entre dans la construction d'un objet fabriqué

Forme : Qualité d'un objet, résultant de son organisation interne, de sa structure, concrétisée par les lignes et les surfaces qui le délimitent, susceptible d'être appréhendée par la vue et le toucher, et permettant de le distinguer des autres objets indépendamment de sa nature et de sa couleur

Corps : Ensemble des parties matérielles constituant l'organisme, siège des fonctions physiologiques et, chez les êtres animés, siège de la vie animale

Mouvement : Courant de pensée qui marque un changement des idées dans le domaine artistique, intellectuel, littéraire, etc

Style : Ensemble des moyens d'expression (vocabulaire, images, tours de phrase, rythme) qui traduisent de façon originale les pensées, les sentiments, toute la personnalité d'un auteur.

Courant : Suivre plus ou moins passivement les habitudes qui semblent l'emporter autour de soi, dans la société de son temps ; Déplacement de personnes dans une même direction

Période : Espace de temps plus ou moins long, phase marquée par un fait, un événement, une situation, des caractères précis et se reproduisant dans certains cas

Oeuvre, une : Ensemble d'actions accomplies par quelqu'un en vue d'un certain résultat

Oeuvre, un : Tâche, entreprise, action.

Icone : Dans la classification de Peirce : type de signe qui opère par similitude de fait entre deux éléments; p. ex. : le dessin représentant une maison et la maison représentée`` (Media 1971)

Iconographie : Étude méthodique des représentations plastiques (peintures, sculptures, gravures) d'un sujet donné (personne, époque, thème, symbole, lieu, civilisation, religion), avec leurs sources, leurs significations et leur classement

Iconologie : Science de la représentation dans les arts des figures allégoriques, mythiques et emblématiques, et de leurs attributs; p. méton., répertoire de ces représentations.

Semiologie : Étude des systèmes de communication (incluant ou non les langues naturelles) posés et reconnus comme tels par l'institution sociale (code de la route, signaux maritimes, etc.) ; Étude des pratiques signifiantes, des significations attachées aux faits de la vie sociale et conçus comme systèmes de signes

Espace : Distance comprise entre un point et un autre, entre un lieu, un objet et un autre ; Milieu idéal indéfini, dans lequel se situe l'ensemble de nos perceptions et qui contient tous les objets existants ou concevables (concept philosophique dont l'origine et le contenu varient suivant les doctrines et les auteurs).

Expérience : Fait d'acquérir, volontairement ou non, ou de développer la connaissance des êtres et des choses par leur pratique et par une confrontation plus ou moins longue de soi avec le monde.

Expérimentation : Méthode scientifique exigeant l'emploi systématique de l'expérience afin de vérifier les hypothèses avancées et d'acquérir des connaissances positives dans les sciences expérimentales

Exposition : Action de disposer de manière à mettre en vue ; Présentation publique, pour une durée déterminée en un certain lieu, de produits agricoles, manufacturés ou d'œuvres d'art

Objet : Tout ce qui, animé ou inanimé, affecte les sens, principalement la vue ; Chose solide, maniable, généralement fabriquée, une et indépendante, ayant une identité propre, qui relève de la perception extérieure, appartient à l'expérience courante et répond à une certaine destination

Fiction : Produit de l'imagination qui n'a pas de modèle complet dans la réalité ; Construction imaginaire consciente ou inconsciente se constituant en vue de masquer ou d'enjoliver le réel

Réalité : Aspect physique (des choses). ; Ce qui existe indépendamment du sujet, ce qui n'est pas le produit de la pensée

Imaginaire  : Créé par l'imagination, qui n'a d'existence que dans l'imagination. ; Domaine de l'imagination, posé comme intentionnalité de la conscience (JP Sartre) ; [selon Lacan] Registre essentiel (avec le réel et le symbolique) du champ psychanalytique, caractérisé par la prévalence de la relation à l'image du semblable (d'apr. Lapl.-Pont. 1967) 

Image : Représentation (ou réplique) perceptible d'un être ou d'une chose ; Représentation de la forme ou de l'aspect d'un être ou d'une chose

Aspect : Fait de s'offrir aux yeux, à la vue ou à l'esprit ; Manière dont une personne ou une chose se présente à la vue ou à l'esprit

Dispositif : Manière dont sont disposées, en vue d'un but précis, les pièces d'un appareil, les parties d'une machine; p. méton., mécanisme, appareil ; Ensemble d'éléments agencés en vue d'un but précis 

Installation : Fait d'installer quelqu'un ou de s'installer ; Action d'aménager un local ou un ensemble en vue d'un usage déterminé

Performance : Épreuve non verbale permettant d'apprécier l'intelligence concrète, pratique d'un individu ; Épreuve non verbale permettant d'apprécier l'intelligence concrète, pratique d'un individu

Sculpture : Action de tailler une matière dure, de façonner une matière selon des techniques appropriées, d'assembler divers matériaux, afin de dégager, dans un but utilitaire ou esthétique, un objet, une figure, un ornement; ensemble des techniques utilisées à cet effet

Dessin : Art de représenter des objets (ou des idées, des sensations) par des moyens graphiques; p. méton., ensemble des procédés relatifs à cet art.

Peinture : Matière colorante composée d'un pigment et d'un liant, utilisée pour recouvrir une surface, pour la protéger ou l'orner ; echnique, procédé, pratique d'un genre de l'art pictural (défini par l'emploi de tel support, de telle matière, par la mise en oeuvre de certains sujets, de certaines conceptions)

Gravure : Travail d'art, d'artisanat ou d'industrie utilisant l'incision ou le creusement, à l'aide d'un instrument tranchant ou d'un mordant, pour confectionner un élément imprimant, en relief ou en creux, destiné à la reproduction d'une image ou d'un texte par impression ou par frappage

Vidéo : Technique ou ensemble de techniques permettant la formation, l'enregistrement, le traitement, la transmission ou la reproduction d'images de télévision ou d'images analogues ou de signaux occupant une largeur de bande comparable sur un écran de visualisation ; 

Enregistrer : Inscrire sur un registre de manière à conserver (une information) ; ranscrire un acte ou un jugement sur un registre public afin d'en assurer l'authenticité ; Recueillir et conserver (des sons) sur un support matériel

Produire : Faire exister, naturellement ou non, ce qui n'existe pas encore; créer

Geste : Mouvement extérieur du corps (ou de l'une de ses parties), perçu comme exprimant une manière d'être ou de faire (de quelqu'un)

Acte : Manifestation concrète des pouvoirs d'agir d'une personne, ce que fait une personne

Intégration : Action d'incorporer un ou plusieurs éléments étrangers à un ensemble constitué, d'assembler des éléments divers afin d'en constituer un tout organique; passage d'un état diffus à un état constant; résultat de l'action

Ecran : Surface faisant arrêt, sur laquelle peut apparaître l'image d'un objet ; Surface courbe, fond du tube cathodique, permettant de voir des images transmises par ondes électromagnétiques

Cadre : Bordure de bois, de métal, de marbre, etc., qui entoure un tableau, un miroir, une photographie... tout autre objet qu'elle protège et décore ; Limite spatiale d'une scène de film ; Milieu physique ou humain dans lequel se déroule habituellement l'existence et l'activité d'une personne, d'un groupe

Piédestal : Support isolé d'une statue, d'une colonne, d'un élément décoratif

Socle : Assise unie ou moulurée, le plus souvent quadrangulaire sur laquelle repose un édifice, une colonne ou qui sert de support à une statue, une pendule

Inventer : Trouver par la force de l'imagination créatrice et réaliser le premier quelque chose de nouveau

Narration : Relation détaillée, écrite ou orale (d'un fait, d'un événement) ; Partie du discours où l'orateur raconte, expose, développe le fait

Storyboard : Partie du discours où l'orateur raconte, expose, développe le fait

Bande-dessinée : principale application de l'art séquentiel au support papier (Will Esner), juxtaposition de dessins (ou d'autres types d'images fixes, mais pas uniquement photographiques), articulés en séquences narratives et le plus souvent accompagnés de textes

Film : Succession d'événements qui se déroulent dans le temps comme les images au cours d'un film; récit chronologique d'un événement particulier.

Cinéma : Ensemble des activités liées à la réalisation des films et à leur commercialisation ; Procédé permettant l'enregistrement et la projection animée de vues, accompagnée ou non de son

Roman-photo : Histoire entre plusieurs personnages, généralement d'un romanesque convenu, mélodramatique, racontée dans une succession de photographies évocatrices, explicitées par des dialogues et des commentaires brefs (bulles et légendes) et publiée dans des magazines populaires

Support : Action de soutenir, de donner son appui moral ou matériel à quelqu'un ou à quelque chose; résultat de cette action ; Objet, élément matériel sur lequel repose une chose, en particulier un objet pesant, destiné à servir d'appui ou de soutien à ce dernier.

Médium : Ce qui occupe une position moyenne, ce qui constitue un état, une solution intermédiaire ; ce qui sert d'intermédiaire, ce qui produit une médiation entre émetteur et récepteur

Outil : Objet fabriqué, utilisé manuellement, doté d'une forme et de propriétés physiques adaptées à un procès de production déterminé et permettant de transformer l'objet de travail selon un but fixé ; Moyen; ce qui permet d'obtenir un résultat, d'agir sur quelque chose

Instrument : Objet fabriqué en vue d'une utilisation particulière pour faire ou créer quelque chose, pour exécuter ou favoriser une opération (dans une technique, un art, une science) ; Moyen, agent, intermédiaire servant à atteindre une fin

Schéma : Représentation graphique réduite à l'essentiel, et souvent symbolique, mais où toutes les informations se trouvent données de façon précise ; Cette représentation des constituants fondamentaux d'un objet complexe, incluant les relations fonctionnelles existant entre ces constituants

Esquisse : Première étude d'une composition picturale, sculpturale, architecturale, indiquant les grandes lignes du projet et servant de base à son exécution définitive ; Ébauche, commencement d'un geste, d'une action

Croquis : Représentation figurative (d'un sujet) réduite à ses éléments essentiels

Détournement : Action de tourner quelque chose dans une autre direction ; 

Fabrication : Art, action de fabriquer (cf. ce mot A 1); résultat de cette action

Fabriquer : Faire, réaliser (un objet), une chose applicable à un usage déterminé, à partir d'une ou plusieurs matières données, par un travail manuel ou artisanal.

Transformation : Action de transformer; résultat de cette action ; Changement complet de caractère, de manière d'être

Transformer : Rendre différent, faire devenir autre, modifier entièrement ; Donner une autre forme à

Prélèvement : Action de prélever une portion de quelque chose sur un tout, une partie d'un ensemble; p.méton., résultat de cette action

Citation : Action de citer un passage d'auteur, de reproduire exactement ce qu'il a dit ou écrit, oralement ou dans un texte ; Faits, circonstances que l'on rapporte, que l'on avance à l'appui de son opinion

Emprunt : Fait de prendre quelque chose pour se l'approprier, pour l'utiliser ou l'imiter

Assemblage : Action de mettre ensemble, de réunir; résultat de cette action

Assembler : Action de mettre ensemble, de réunir; résultat de cette action

Collage : Action de coller des choses quelconques, résultat de cette action ; Composition surréaliste ou cubiste exécutée au moyen de diverses matières (le plus souvent papiers découpés) collées sur une toile ou intégrées à une partie peinte

Coller : Joindre et fixer deux choses ensemble en se servant de colle

Volume : Corps, objet appréhendé dans ses trois dimensions; représentation qui en est faite, par des procédés graphiques ou plastiques; impression de relief, de masse, de profondeur qui en résulte ; Espace apparent qu'occupe un corps, un objet; encombrement causé par ce corps, cet objet

Tridimensionnel : Qui se développe dans un espace à trois dimensions ; (espace) espace qui nous entoure, tel que perçu par notre vision, en termes de largeur, hauteur et profondeur

3D : représentation en images de synthèse (numérique), le relief des images stéréoscopiques ou autres images en relief, et même parfois le simple effet stéréophonique, qui ne peut par construction rendre que de la 2D

Bidimenssionnel : Qui se développe dans un espace à deux dimensions ; (espace) espace divisé en deux dimension, en termes de largeur et de hauteur 

Durée : Continuité indéfinie du temps, du devenir ; Expérience du temps subjectif, vécu par la conscience, en dehors de toute conceptualisation et envisagé qualitativement, par opposition au temps objectif, mesurable, mathématique de la science

Vitesse : Action ou faculté de se déplacer rapidement, de parcourir un grand espace en un minimum de temps ; Temps mis pour parvenir à son entier accomplissement ; Fait d'accomplir un acte, de réaliser quelque chose en un minimum de temps.

Rythme : Répétition périodique (d'un phénomène de nature physique, auditive ou visuelle)

Découpage : Action de détacher en coupant suivant un contour précis

Ellipse : Omission d'un ou plusieurs mots dans un énoncé dont le sens reste clair

Montage : Opération qui consiste à assembler les divers éléments d'un objet en vue de son utilisation ; Action de porter ou de mettre quelque chose dans un endroit plus élevé ; 

Référent : Ce à quoi le signe linguistique renvoie soit dans la réalité extra-linguistique ou univers réel, soit dans un univers imaginaire`` (Lang. 1973)

Infographie : Application de l'informatique à la représentation graphique et au traitement de l'image

Pérennité : État, caractère de ce qui dure toujours ou très longtemps

Instantanéité : Caractère de ce qui se produit en un (très) petit espace de temps, subitement, immédiatement.

Instantané : Qui se produit en un (très) petit espace de temps, subitement, immédiatement

éphémère : Qui dure peu de temps, qui (s')échappe, qui ne fait que passer ; Qui dure peu

Vue : Enregistrer l'image de ce qui se trouve dans le champ visuel, d'une manière passive, sans intention préalable; en percevoir la forme, la couleur, la position, le mouvement.

Point de vue : Enregistrer l'image de ce qui se trouve dans le champ visuel, d'une manière passive, sans intention préalable; en percevoir la forme, la couleur, la position, le mouvement depuis une certains angle.

Angle : Pour un point donné, par rapport à un observateur, c'est l'angle que forme, avec le plan horizontal, la droite joignant l'observateur au point choisi.`` (Uv.-Chapman 1956)

Profondeur : Caractère de ce qui est profond ; Effet produisant un espace à trois dimensions à partir d'un support à deux dimensions; espace fictif, troisième dimension créé(e) par la perspective ; éciproque, appliquée à l'espace-image, de la notion de profondeur de champ (...). L'écart entre les deux positions extrêmes est la profondeur de foyer. Elle est proportionnelle à l'inverse de l'ouverture relative`` (Mill. Vision 1981)

Son : Sensation auditive produite sur l'organe de l'ouïe par la vibration périodique ou quasi-périodique d'une onde matérielle propagée dans un milieu élastique, en particulier dans l'air; p. méton., cette onde matérielle; ce qui frappe l'ouïe, avec un caractère plus ou moins tonal ou musical, par opposition à un bruit ; 

Posture : Attitude, position du corps, volontaire ou non, qui se remarque, soit par ce qu'elle a d'inhabituel, ou de peu naturel, de particulier à une personne ou à un groupe, soit par la volonté de l'exprimer avec insistance ; Attitude morale de quelqu'un

Morale : Qui concerne les règles ou principes de conduite, la recherche d'un bien idéal, individuel ou collectif, dans une société donnée

Plein : Qui est tout entier rempli de la matière, de la substance qui lui est propre ; Être rempli à l'extrême ; Qui contient le maximum de choses ou de personnes

Vide : Qui ne contient rien de concret; p. ext., qui est dépourvu de son contenu

In situ : Dans son cadre naturel, à sa place normale, habituelle.

Masse : L'ensemble physique que constitue dans l'espace cette chose, cet objet, caractérisé par son volume, sa rigidité, sa densité, etc ; Chose, objet qui a perdu sa forme ; L'ensemble continu, solide ou plus ou moins cohérent, que constituent les éléments matériels dont sont formés les corps naturels

Monument : Ouvrage d'architecture ou de sculpture édifié pour transmettre à la postérité le souvenir d'une personne ou d'un événement ; Objet qui atteste l'existence, la réalité de quelque chose et qui peut servir de témoignage

Mise en espace : exprime un niveau de représentation qui n'atteint pas la sophistication d'une mise en scène complète (source wikipédia)

Environnement : Ensemble des choses qui se trouvent aux environs, autour de quelque chose ; Contexte, en particulier contexte immédiat

Idéisme : Dont le but est l'expression de l'idée (ou des idées)

Idée : Tout ce qui est représenté dans l'esprit, par opposition aux phénomènes concernant l'affectivité ou l'action

sources : CNRTL , [En ligne] http://www.cnrtl.fr/, [Consulté le 21/04/2015]

## Les principaux courants artistiques du XXème siècle

1860 : L'Impressionisme 

Le tableau « Impression soleil levant » de Claude Monet en 1872 sera à l’origine du terme « impressionnisme ».

Les principaux artistes sont : Claude Monet, Auguste Renoir, Camille Pissarro, Alfred Sisley, Berthe Morisot et Paul Cézanne, Edgar Degas… 
Malgré leurs styles différents, ces artistes refusent tout ce qui est cher à la peinture officielle comme les sujets historiques, mythologiques, sentimentaux, ainsi que la facture « léchée ».
Ils empruntent leurs sujets à la réalité ordinaire de leur époque, et fixent sur la toile ce que l’on perçoit à l’instant précis où l’on peint.

1880 : les Naïfs
Les amateurs ont accès à la culture sous forme de cours du soir. Naît le salon des indépendants sans jury, avec la participation d’Henri Rousseau dit le Douanier Rousseau. 

1886 - 1900 : Le Symbolisme
L’œuvre d’art est idéiste et symbolique.

Les principaux artistes de ce mouvement sont : Paul Gauguin, Puvis de Chavannes, Gustave Moreau, Odilon Redon, Edwards Burnes Johns et Gabriel Rossetti.

De ce courant découle l’école de Pont-Aven, cloisonnisme, influence de l’estampe japonaise et du plomb des vitraux. Les formes sont synthétisées et ondulantes.
Le groupe des Nabis (prophètes en hébreu) est animé par Paul Sérusier à partir de 1888. Son tableau « le Talisman », réalisé sous la dictée de Paul Gauguin, est le point de départ d’une libération abstraite de la couleur.

1895 - 1910 : l'Art Nouveau (ou style 1900)
La ligne ondulante schématise les formes en les cernant. L’Art Nouveau s’inspire du monde végétal et de l’estampe japonaise, redécouverte à la fin du XIXème siècle.

Les principaux artistes sont : Émile Gallé (verrerie), Antonio gaudi (architecture), Victor Horta, René Lalique (verrerie), Hector Guimard (bouches de métro) et Alfons Mucha (publiciste de Sarah Bernhardt).

1900 : l'Expressionnisme
Alors que les impressionnistes se contentent de la sensation visuelle, les expressionnistes l’enrichissent de la vie psychologique. L’artiste n’est plus celui qui sait, mais celui qui exprime. La spontanéité l’emporte sur la réflexion, l’originalité sur le style.

Les principaux artistes sont : Georges Rouault en France, Ernst Ludwig Kirchner en Allemagne, inspirateur du groupe « Die Brücke » et Edward Munch. En Autriche, avec Oskar Kokoschka et Egon Schiele. En Belgique avec James Ensor.

OEUVRES
: Edvard MUNCH « le cri »,1893
, Nasjonalgalleriet à Oslo (Norvège); James ENSOR «l’entrée du Christ à Bruxelles », 1888, Getty Museum, Los Angeles (USA). 

1905 : le Fauvisme 
Selon Matisse, le fauvisme est une recherche d’intensité dans la couleur dans laquelle est absente la perspective.

Les principaux artistes sont : Henri Matisse, André Derain et Maurice de Vlaminck.

OEUVRES
Henri MATISSE, « la Desserte, harmonie rouge »,1908, huile sur toile, Musée de l’Hermitage, St Petersburg, Russie. 

1908 : le Cubisme 
École d’art, florissante de 1910 à 1930, se propose de représenter les objets décomposés en éléments géométriques simples (rappelant le cube), sans restituer leur perspective.

Les principaux artistes sont Pablo Picasso et Georges Braque.

La peinture va s’épanouir avec les collages et l’intégration d’éléments étrangers à la peinture comme des papiers imprimés, des lettres, …

OEUVRES: 
Pablo PICASSO (1881-1973) « les demoiselles d’Avignon », 1907, museum of Modern Art, New York 
Georges BRAQUE « Nature morte sur une table: Gillette », 1914, musée d’art moderne, Paris

1910 : Futurisme
Doctrine esthétique formulée par le poète italien Marinetti, exaltant le mouvement, et tout ce qui dans le présent (vie ardente, vitesse, machinisme, révolte, goût du risque….) préfigurait le monde futur.

Les principaux artistes sont Giacomo Balla, Umberto Boccioni, Gino Severini…

Expressionnisme, cubisme, futurisme… les groupements, les noms, les tendances varient selon les pays et les hommes, mais le besoin de changer l’art reste le même.

OEUVRES : 
Umberto BOCCIONI (1882-1916) « formes uniques dans la continuité de l’espace », 1913, bronze, galerie d’art moderne de Milan, Italie

1910 : Abstraction
C’est Kandinsky qui est à l’origine de ce mouvement. Volonté de ne pas représenter le monde (réel ou imaginaire) et de n’utiliser la matière, la ligne, la couleur que pour elles-mêmes.

Les principaux artistes sont Piet Mondrian, Kasimir Malevitch, Paul Klee, Robert Delaunay.

OEUVRES: 
KANDINSKY (1866-1944) « Aquarelle abstraite », 1910, musée national d’art moderne, Paris 
Piet MONDRIAN (1872-1944) « Composition II », 1937, musée national d’art moderne, Paris 

1916: Dada
Max Ernst devient le fondateur du dadaïsme, vite rejoint par Arp, Picabia, Duchamp, Otto Dix. C’est un mouvement littéraire et artistique révolutionnaire, réagissant à l’absurdité de la guerre de 1914-1918.

OEUVRES:
Otto DIX (1891-1969) « les joueurs de cartes »,1920, œuvre disparue

1920: Surréalisme
Il fait suite au mouvement « Dada ». Les surréalistes sont influencés par les doctrines psychanalytiques. Ils tentent de transcrire les messages ténébreux de l’inconscient. 

Les principaux artistes sont Salvador Dali, Giorgio De Chirico, René Magritte….

OEUVRES:
Salvador DALI (1904-1989) « Hallucinations partielle. Six images de Lénine sur un piano », 1931, musée national d’art moderne, Paris 
René MAGRITTE(1898-1967) « Le Double Secret », 1927, musée national d’art moderne, Paris 
Salvador DALI "lesmontres molles", 1931, Musée d'art moderne de New York

1925: Art Déco
Ce mouvement doit son nom à l’exposition internationale des arts décoratifs et industriels modernes, qui a lieu en 1925 à Paris. En ce début de XXème siècle, alors que l’industrialisation poursuit son expansion, les formes ondoyantes et sensuelles de l’Art nouveau font place aux rythmes saccadés de l’Art Déco. Ses formes géométriques, ses couleurs chatoyantes et l’utilisation de matériaux modernes comme le plastique, s’intègrent dans l’esthétique industrielle naissante.

Les principaux artistes sont : Marcel Bouraine (pâtes de cristal), Paul Manship (sculpture), Georges Lepape, Paul Colin, Charles Gesmar, Cassandre…

1940: Action Painting
 Il invente le 1er art né aux USA. Sa technique est particulière car il pose une immense toile au sol et jette de la peinture dessus à l'aide d'un bâton qui lui s ert de pinceau. Parfois, il marche sur la toile avec un pot de peinture percé. Cet art appartient au surréalisme, c'est (expression en anglais en deux mots) l'action painting.

 Artiste Principal : Jackson POLLOCK(1912-1956)

OEUVRES: 
Jackson POLLOCK, "Painting. Silver over Black, White, Yellow and Red"1948, musée national d'art moderne, Paris

1945: Art Brut
Cet art a été inventé par Jean DUBUFFET en 1945. Il désigne les œuvres faites par des personnes qui vivent à l’écart de la société, dans les hôpitaux psychiatriques. Dans ses propres œuvres, il utilise des techniques et des matériaux variés : terre, ciment, colle, ailes de papillon, émail, chiffons, ... C’est l’art brut 

Artistes Principaux : Jean DUBUFFET

OEUVRES : 
Jean DUBUFFET « le danseur »,1954, éponge, filasse, brique et ciment, musée national d’art moderne, Paris 

1950: Pop Art
Origine en Grande Bretagne au milieu des années 1949, sous l'impulsion de Richard Hamilton et Eduardo Paolozzi. Un peu plus tard, au tout début des années 1960, c'est au tour du Pop art américain d'émerger. C'est surtout la branche américaine qui va populariser ce courant artistique devenu majeur, en questionnant la consommation de masse de façon agressive. Il s'agit principalement de présenter l'art comme un simple produit à consommer ; éphémère,jetable, bon marché..

Artiste Principaux : Andy Warhol, Roy Lichtenstein

OEUVRES : 
Warhol, "Marylin", 1964, MNAM, Paris
Roy Lichtenstein, "Blam", 1964

1960: Nouveau Réalisme
Fondé par le peintre Yves Klein. Ce courant contemporain du Pop Art américain est présenté comme la version française. Le nouveau Réalisme incarne l'une des nombreuses tendances de l'avant-garde des années 1960. Il se base sur l'utilisation d'objet prélevé dans la réalité del eur temps, à l'image du ready-made de Marcel Duchamp. C'est l'art de l'assemblage et de l'accumulation.

Artiste principaux: Christo, César, Yves Klein

OEUVRES:
Christo, "Le Reichstag emballé", 1971-1995, Berlin
César, "Pouce", 1965, fonte d'acier, H 6m

1967: Arte Povera
De l'italien "Art pauvre", l'arte Povera et une attitude prônée par des artiste italiens depuis 1967. Elle consiste à défier l'industrie culturelle et plus largement la société de consommation en privilégiant le processus, le geste créateur au détriment de l'objet fini. "Rendre signifiant les objets insignifiants".
L'arte Povera utilise des produits pauvres comme le sable, le chiffon, la toile, la corde, le goudron, les vêtements usés, etc. et les positionne comme les éléments d'une composition.

Artiste principaux : Mario Merz, Giovanni Anselmo, Jannis Kounellis, Pino Pascali, Michelangleo Pistoletto

OEUVRES:
Giovanni Anselmo, "Senzo titolo" (Struttura che mangia), 1968
Jannis Kounellis, "Senza titolo", 1969
Mario Merz, "Igloo de Giap", 1968
Pino Pascali, "Le penne di Esopo", 1968
Giuseppe Penone, "Soffio 6", 1978
Michelangelo Pistoletto, "Metrocubo d’infinito", 1965-66
Gilberto Zorio, "Per purificare le parole", 1969

1970: Land Art
Le Land Art utilise le cadre et les matériaux de la nature(sable, bois, terre, pierres, rocher...). Le plus souvent les oeuvres sont exposées à l'extérieur, exposées aux éléments et soumises à l'érosion naturelle ; ainsi certaine ont disparu et il ne reste que leur souvenir photographique. La nature est plus simplement représentée mais c'est au coeur d'elle-même(in situ) que les créateur travaille. Ils aspirent a quitter les galeries avec leurs tickets d'entrées et leurs heures d'ouvertures.

Artistes principaux: Robert Smithson, Michael Hazer, James Turrel, Richard Long, Shireko Hirakawa...

OEURVRES:
Robert Smithson, "Spiral Jetty", Grands lacs salés , Utah, USA
Richar Long, "Rock Circle", Sahara
