kitdesurvie.controller('mouvementController', ['$scope',"kitdesurvieService", function($scope, kitdesurvieService) {
  var mouvementController = this;
  mouvementController.mouvements = [];
  mouvementController.selectedObjectIndex = null;
  mouvementController.allOeuvres = [];
  $scope.searchingTerms = [];

  mouvementController.selectObject = function($index) {
      mouvementController.selectedObjectIndex = $index;
  }

  var genID = function() {
    var randLetter = String.fromCharCode(65 + Math.floor(Math.random() * 26)),
        combi = randLetter + Date.now();
    return combi
  };

  mouvementController.addMouvement = function() {
    toggleOverlay();
    animateMouvementPopUp(true);
  };

  mouvementController.sendMouvement = function() {
    event.preventDefault();
    event.stopPropagation();
    var newMouvement = {
      title:mouvementController.title,
      date: mouvementController.date,
      description:mouvementController.description,
      oeuvres:mouvementController.allOeuvres
    }
    if((typeof newMouvement.title !== "undefined" && newMouvement.title !== "") && (typeof newMouvement.description !== "undefined" && newMouvement.description !== "") && (typeof newMouvement.date !== "undefined" && newMouvement.date !== "")) {
      kitdesurvieService.pushData("mouvements",newMouvement,
        function(data){
          mouvementController.title = "";
          mouvementController.date = "";
          mouvementController.description = "";
          // animateMouvementPopUp(true);
          loadRemoteData();
          mouvementController.allOeuvres = [];
      },function(error){
        console.log(error);
      });
    }
  }

  mouvementController.removeData = function() {
    kitdesurvieService.removeData("mouvements",event.target.getAttribute("data-id"),
        function(data){
        loadRemoteData();
      },function(error){
        console.log(error);
    });
  };

  mouvementController.promptModifyData = function($index) {
    event.preventDefault();
    toggleOverlay();
    mouvementController.selectObject($index);
    animateMouvementPopUp(false);
  }

  mouvementController.modifyData = function() {
    var dataToChange = mouvementController.mouvements[mouvementController.selectedObjectIndex];
    if(mouvementController.allOeuvres.length > 0){
      mouvementController.allOeuvres.forEach(function(oeuvre){
        dataToChange.oeuvres.push(oeuvre);
      });
    };
    kitdesurvieService.modifyData("mouvements",dataToChange.id,dataToChange,
        function(data){
        loadRemoteData();
        mouvementController.allOeuvres = [];
      },function(error){
        console.log(error);
      });
  };

  mouvementController.addOeuvre = function() {
    event.preventDefault();
    animateOeuvrePopUp2();
    // console.log(mouvementController.allOeuvres);
  };

  mouvementController.deleteOeuvre = function($index) {
    event.preventDefault();
    var mouvementId = event.target.getAttribute("data-id");
    mouvementController.mouvements.forEach(function(mouv){
      if(mouv.id == mouvementId) {
        var oeuvre = mouv.oeuvres.splice($index, 1);
        return kitdesurvieService.modifyData("mouvements",mouv.id,mouv,
            function(data){
            loadRemoteData();
            mouvementController.allOeuvres = [];
          },function(error){
            console.log(error);
          });
      }
    })
  };

  mouvementController.saveOeuvre = function() {
    var target = event.target;
    if((mouvementController.oeuvreTitle !== "" && mouvementController.oeuvreTitle !== "undefined") && (mouvementController.oeuvreDate !== "" && mouvementController.oeuvreDate !== "undefined") && (mouvementController.oeuvreDescription !== "" && mouvementController.oeuvreDescription !== "undefined") && (mouvementController.oeuvreUrl !== "" && mouvementController.oeuvreUrl !== "undefined")) {
      mouvementController.allOeuvres.push({
        title : mouvementController.oeuvreTitle,
        date : mouvementController.oeuvreDate,
        description : mouvementController.oeuvreDescription,
        url : mouvementController.oeuvreUrl
      });
      mouvementController.oeuvreTitle = "";
      mouvementController.oeuvreDate = "";
      mouvementController.oeuvreDescription = "";
      mouvementController.oeuvreUrl = "";
    }
    console.log(mouvementController.allOeuvres);
  }

  // I apply the remote data to the local scope.
  function applyRemoteData( queriedData ) {
      mouvementController.mouvements = queriedData;
  }
  // I load the remote data from the server.
  function loadRemoteData() {
      // The friendService returns a promise.
      kitdesurvieService.getData("mouvements?_sort=title&_order=ASC",
          function(data){
          applyRemoteData( data.data );
        },function(error){
          console.log(error);
        });
  };

  // function addSelectAttribute() {
  //   var wrapper = _(".item-wrapper")[1],
  //       selectedDivs = Array.prototype.slice.call(wrapper.children),
  //       scrollHeight = wrapper.scrollHeight,
  //       scrollTop = wrapper.scrollTop;
  //
  //   wrapper.addEventListener("scroll", function(event){
  //     down = typeof down == "undefined" ? false : down;
  //     if(this.scrollTop >= window.innerHeight && window.innerWidth < 800 && !down) {
  //       _(".searchbar")[1].style.transform = "translate(0,0)";
  //       _(".searchbar")[1].style.webkitTransform = "translate(0,0)";
  //       down = true;
  //     } else if(this.scrollTop < window.innerHeight && window.innerWidth < 800 && down){
  //       _(".searchbar")[1].style.transform = "translate(0,-100px)";
  //       _(".searchbar")[1].style.webkitTransform = "translate(0,-100px)";
  //       down = false;
  //     }
  //     });
  //
  //   wrapper.addEventListener("scroll", function(event){
  //     selectedDivs.forEach(function(item,index){
  //       if(index > 2 && item.type !=="submit" ) {
  //         var boundingRect = item.getBoundingClientRect();
  //         if(window.innerWidth > 800) {
  //           if((scrollTop+scrollHeight/2-boundingRect.height) >= boundingRect.top && (scrollTop+scrollHeight/2-boundingRect.height) <= boundingRect.bottom) {
  //             // console.log(scrollTop);
  //             item.style.backgroundColor = "#c1ede8";
  //             item.style.color = "rgb(100, 102, 103)";
  //           } else {
  //             item.style.backgroundColor = "white";
  //             item.style.color = "black";
  //           }
  //         } else {
  //           if(scrollTop+(window.innerHeight/2) >= boundingRect.top && scrollTop+(window.innerHeight/2) <= boundingRect.bottom) {
  //             item.style.backgroundColor = "#c1ede8";
  //             item.style.color = "rgb(100, 102, 103)";
  //           } else {
  //             item.style.backgroundColor = "white";
  //             item.style.color = "black";
  //           }
  //         }
  //       }
  //     });
  //   })
  //
  // };

  function normalizeString(str) {
    var nStr = "";
    nStr = str.replace(/\s|\[|\]|\{|\}/gim,"");
    nStr = str.replace(/-/gim,"\-");
    nStr = str.replace(/\./gim,"\.");
    nStr = str.replace(/\s/gim,"");
    return nStr;
  }

  loadRemoteData();
  // window.setTimeout(addSelectAttribute, 500);
  toggleResearchBar();
}]);
kitdesurvie.directive('mouvements', function() {
  return {
    templateUrl: 'js/directives/mouvements.html'
  };
});
