kitdesurvie.controller('artisteController', ['$scope',"kitdesurvieService", function($scope, kitdesurvieService) {
  var artisteController = this;
  artisteController.artistes = [];
  artisteController.selectedObjectIndex = null;
  artisteController.allOeuvres = [];
  $scope.searchingTerms = [];

  artisteController.selectObject = function($index) {
      artisteController.selectedObjectIndex = $index;
  }

  var genID = function() {
    var randLetter = String.fromCharCode(65 + Math.floor(Math.random() * 26)),
        combi = randLetter + Date.now();
    return combi
  };

  artisteController.addArtiste = function() {
    toggleOverlay();
    animateArtistePopUp(true);
  };

  artisteController.sendArtiste = function() {
    event.preventDefault();
    event.stopPropagation();
    var newArtiste = {
      title:artisteController.title,
      date: artisteController.date,
      description:artisteController.description,
      oeuvres:artisteController.allOeuvres
    }
    if((typeof newArtiste.title !== "undefined" && newArtiste.title !== "") && (typeof newArtiste.description !== "undefined" && newArtiste.description !== "") && (typeof newArtiste.date !== "undefined" && newArtiste.date !== "")) {
      kitdesurvieService.pushData("artistes",newArtiste,
        function(data){
          artisteController.title = "";
          artisteController.date = "";
          artisteController.description = "";
          // animateArtistePopUp(true);
          loadRemoteData();
          artisteController.allOeuvres = [];
      },function(error){
        console.log(error);
      });
    }
  }

  artisteController.removeData = function() {
    kitdesurvieService.removeData("artistes",event.target.getAttribute("data-id"),
        function(data){
        loadRemoteData();
      },function(error){
        console.log(error);
      });
  };

  artisteController.promptModifyData = function($index) {
    event.preventDefault();
    toggleOverlay();
    artisteController.selectObject($index);
    animateArtistePopUp(false);
  }

  artisteController.modifyData = function() {
    var dataToChange = artisteController.artistes[artisteController.selectedObjectIndex];
    if(artisteController.allOeuvres.length > 0){
      artisteController.allOeuvres.forEach(function(oeuvre){
        dataToChange.oeuvres.push(oeuvre);
      });
    };
    kitdesurvieService.modifyData("artistes",dataToChange.id,dataToChange,
        function(data){
        loadRemoteData();
        artisteController.allOeuvres = [];
      },function(error){
        console.log(error);
      });
  };

  artisteController.addOeuvre = function() {
    event.preventDefault();
    animateOeuvrePopUp();
    // console.log(artisteController.allOeuvres);
  };

  artisteController.deleteOeuvre = function($index) {
    event.preventDefault();
    var artisteId = event.target.getAttribute("data-id");
    artisteController.artistes.forEach(function(arti){
      if(arti.id == artisteId) {
        var oeuvre = arti.oeuvres.splice($index, 1);
        return kitdesurvieService.modifyData("artistes",arti.id,arti,
            function(data){
            loadRemoteData();
            artisteController.allOeuvres = [];
          },function(error){
            console.log(error);
          });
      }
    })
  };

  artisteController.saveOeuvre = function() {
    var target = event.target;
    if((artisteController.oeuvreTitle !== "" && artisteController.oeuvreTitle !== "undefined") && (artisteController.oeuvreDate !== "" && artisteController.oeuvreDate !== "undefined") && (artisteController.oeuvreDescription !== "" && artisteController.oeuvreDescription !== "undefined") && (artisteController.oeuvreUrl !== "" && artisteController.oeuvreUrl !== "undefined")) {
      artisteController.allOeuvres.push({
        title : artisteController.oeuvreTitle,
        date : artisteController.oeuvreDate,
        description : artisteController.oeuvreDescription,
        url : artisteController.oeuvreUrl
      });
      artisteController.oeuvreTitle = "";
      artisteController.oeuvreDate = "";
      artisteController.oeuvreDescription = "";
      artisteController.oeuvreUrl = "";
    }
    console.log(artisteController.allOeuvres);
  }

  // I apply the remote data to the local scope.
  function applyRemoteData( queriedData ) {
      artisteController.artistes = queriedData;
  }
  // I load the remote data from the server.
  function loadRemoteData() {
      // The friendService returns a promise.
      kitdesurvieService.getData("artistes?_sort=title&_order=ASC",
          function(data){
          applyRemoteData( data.data );
        },function(error){
          console.log(error);
        });
  };

  // function addSelectAttribute() {
  //   var wrapper = _(".item-wrapper")[2],
  //       selectedDivs = Array.prototype.slice.call(wrapper.children),
  //       scrollHeight = wrapper.scrollHeight,
  //       scrollTop = wrapper.scrollTop;
  //
  // wrapper.addEventListener("scroll", function(event){
  //   down = typeof down == "undefined" ? false : down;
  //   if(this.scrollTop >= window.innerHeight && window.innerWidth < 800 && !down) {
  //     _(".searchbar")[2].style.transform = "translate(0,0)";
  //     _(".searchbar")[2].style.webkitTransform = "translate(0,0)";
  //     down = true;
  //   } else if(this.scrollTop < window.innerHeight && window.innerWidth < 800 && down){
  //     _(".searchbar")[2].style.transform = "translate(0,-100px)";
  //     _(".searchbar")[2].style.webkitTransform = "translate(0,-100px)";
  //     down = false;
  //   }
  //   });
  //
  //   wrapper.addEventListener("scroll", function(event){
  //     selectedDivs.forEach(function(item,index){
  //       if(index > 2 && item.type !=="submit" ) {
  //         var boundingRect = item.getBoundingClientRect();
  //         if(window.innerWidth > 800) {
  //           if((scrollTop+scrollHeight/2-boundingRect.height) >= boundingRect.top && (scrollTop+scrollHeight/2-boundingRect.height) <= boundingRect.bottom) {
  //             item.style.backgroundColor = "#f6f6b0";
  //             item.style.color = "rgb(100, 102, 103)";
  //           } else {
  //             item.style.backgroundColor = "white";
  //             item.style.color = "black";
  //           }
  //         } else {
  //           if(scrollTop+(window.innerHeight/2) >= boundingRect.top && scrollTop+(window.innerHeight/2) <= boundingRect.bottom) {
  //             item.style.backgroundColor = "#f6f6b0";
  //             item.style.color = "rgb(100, 102, 103)";
  //           } else {
  //             item.style.backgroundColor = "white";
  //             item.style.color = "black";
  //           }
  //         }
  //       }
  //     });
  //   })
  //
  // };

  function normalizeString(str) {
    var nStr = "";
    nStr = str.replace(/\s|\[|\]|\{|\}/gim,"");
    nStr = str.replace(/-/gim,"\-");
    nStr = str.replace(/\./gim,"\.");
    nStr = str.replace(/\s/gim,"");
    return nStr;
  }

  loadRemoteData();
  // window.setTimeout(addSelectAttribute, 500);
  toggleResearchBar();
}]);
kitdesurvie.directive('artistes', function() {
  return {
    templateUrl: 'js/directives/artistes.html'
  };
});
