(function(){
// Production steps of ECMA-262, Edition 5, 15.4.4.18
// Reference: http://es5.github.io/#x15.4.4.18
if (!Array.prototype.forEach) {

  Array.prototype.forEach = function(callback, thisArg) {

    var T, k;

    if (this == null) {
      throw new TypeError(' this is null or not defined');
    }

    // 1. Let O be the result of calling ToObject passing the |this| value as the argument.
    var O = Object(this);

    // 2. Let lenValue be the result of calling the Get internal method of O with the argument "length".
    // 3. Let len be ToUint32(lenValue).
    var len = O.length >>> 0;

    // 4. If IsCallable(callback) is false, throw a TypeError exception.
    // See: http://es5.github.com/#x9.11
    if (typeof callback !== "function") {
      throw new TypeError(callback + ' is not a function');
    }

    // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
    if (arguments.length > 1) {
      T = thisArg;
    }

    // 6. Let k be 0
    k = 0;

    // 7. Repeat, while k < len
    while (k < len) {

      var kValue;

      // a. Let Pk be ToString(k).
      //   This is implicit for LHS operands of the in operator
      // b. Let kPresent be the result of calling the HasProperty internal method of O with argument Pk.
      //   This step can be combined with c
      // c. If kPresent is true, then
      if (k in O) {

        // i. Let kValue be the result of calling the Get internal method of O with argument Pk.
        kValue = O[k];

        // ii. Call the Call internal method of callback with T as the this value and
        // argument list containing kValue, k, and O.
        callback.call(T, kValue, k, O);
      }
      // d. Increase k by 1.
      k++;
    }
    // 8. return undefined
  };
}

this.Slider = function(container, params) {
  this.container = document.getElementById(container),
  this.duration = params.duration || 0.3,
  this.easing = params.easing || "ease-out",
  this.bulletColor = params.bulletColor || "yellow",
  this.backBulletColor = params.backBulletcolor || "white",
  this.currentSlide = 0,
  this.touch = false,
  this.auto = false
}

var Slide = function(slide, index) {
  this.slide = slide,
  this.index = index
}

var NavElement = function(nav, index) {
  this.nav = nav,
  this.index = index
}

var transform = function(element, transitionX, transitionY) {
  var transitionX = transitionX || 0;
  var transitionY = transitionY || 0;

  element.style.transform = "translate("+transitionX+"px"+","+transitionY+")";
  element.style.WebkitTransform = "translate("+transitionX+"px"+","+transitionY+")";
}

var transition = function(element, property, duration, easing) {
  var property = property || "all";
  var duration = duration || 0;
  var easing = easing || "ease-out";
  var webkitProperty = property == "transform" ? "-webkit-"+property : property;

  element.style.transition = property + " "+ duration +"s "+ easing;
  element.style.WebkitTransition = webkitProperty +" "+ duration +"s "+ easing;
}

Slider.prototype.makeSlider = function() {
  var childs = Array.prototype.slice.call(this.container.children);
  var parent = this;
  var offsetLeft = parseInt(getComputedStyle(this.container.parentElement, null).width);
  var slides = [];
  childs.forEach(function(element,index){
    element.style.position = "absolute";
    transition(element, "transform", parent.duration, parent.easing);
    if(index > 0 && index < childs.length-1) {
      transform(element, offsetLeft);
      offsetLeft += window.innerWidth;
    } else if(index != 0 && index == childs.length-1) {
      transform(element, -offsetLeft);
    }
    slides.push(new Slide(element,index));
  });
  this.slides = slides;
  this.createNav();
  this.on(this.container, "slide", parent.touchMove);
}

Slider.prototype.removeSlider = function() {
  var childs = Array.prototype.slice.call(this.container.children);
  var parent = this;

  var slides = [];
  childs.forEach(function(element,index){
    element.style = "";
  });
  this.container.removeEventListener("slide", parent.touchMove);
  this.container.removeEventListener('touchstart', parent.touchMove);
  this.container.removeEventListener('touchmove', parent.touchMove);
  this.container.removeEventListener('touchend', parent.touchMove);
  this.container.removeEventListener('mousedown', parent.touchMove);
  this.container.removeEventListener('mousemove', parent.touchMove);
  this.container.removeEventListener('mouseup', parent.touchMove);
}

Slider.prototype.onSlide = function() {
}

Slider.prototype.createNav = function() {
  var parent = this;
  var frag = document.createDocumentFragment();
  var navContainer = document.createElement("div");
  var nav = [];
  navContainer.className = "slider-nav-container";

  for(var i = 0; i < this.slides.length; i++) {
    var bullet = document.createElement("div");
    bullet.className = "slider-nav";
    transition(bullet, "background-color", this.duration, this.easing);
    navContainer.appendChild(bullet);
    nav.push(new NavElement(bullet, i));
  }
  frag.appendChild(navContainer);
  this.container.appendChild(frag);
  this.nav = nav;
  this.nav.forEach(function(element, index){
    element.nav.addEventListener("click", function(){
      parent.moveToSlide(index);
    });
  })
}

Slider.prototype.moveToSlide = function(slide, interval) {
  var parent = this;
  var time = interval || 300;
  this.touch = false;

  if(!this.auto) {
    var inter = setInterval(function(){
      parent.currentSlide < slide ? parent.nextSlide() : parent.previousSlide();
      if(parent.currentSlide == slide) {
        window.clearInterval(inter);
        this.touch = true;
      }
    }, time);
  }
}

Slider.prototype.autoSlide = function(slide, interval, callback) {
  var parent = this;
  var time = interval || 300;

  if(!this.touch) {
    parent.auto = true;
    parent.touch = false;

    var inter = setInterval(function(){
      parent.currentSlide < slide ? parent.nextSlide() : parent.previousSlide();
      if(parent.currentSlide == slide) {
        window.clearInterval(inter);
        parent.touch = true;
        parent.auto = false;
      }
    }, time);
    if(callback)callback();
  }
}

Slider.prototype.organizeSlide = function() {
  var parent = this;
  var slides = this.slides;
  var offsetLeft = parseInt(getComputedStyle(this.container.parentElement, null).width);
  var nextSlide = this.slides[this.currentSlide == this.slides.length-1 ? 0 : this.currentSlide+1].slide;
  var previousSlide = this.slides[this.currentSlide == 0 ? this.slides.length-1 : this.currentSlide-1].slide;
  previousSlide.style.visibility = "hidden";
  nextSlide.style.visibility = "hidden";

  transform(previousSlide, -offsetLeft);
  transform(nextSlide, offsetLeft);

  window.setTimeout(function(){
    previousSlide.style.visibility = "visible";
    nextSlide.style.visibility = "visible";
  }, 300);
}

Slider.prototype.nextSlide = function() {
  var parent = this;
  var slide = this.slides[this.currentSlide].slide;
  var nav = this.nav[this.currentSlide].nav;
  var nextSlide = this.slides[this.currentSlide == this.slides.length-1 ? 0 : this.currentSlide+1].slide;
  var nextNav = this.nav[this.currentSlide == this.slides.length-1 ? 0 : this.currentSlide+1].nav;
  var offsetLeft = parseInt(getComputedStyle(this.container.parentElement, null).width);

  transform(slide, -offsetLeft);
  transform(nextSlide, 0);

  nav.style.backgroundColor = parent.backBulletColor;
  nextNav.style.backgroundColor = parent.bulletColor;

  this.currentSlide = this.currentSlide == this.slides.length-1 ? 0 : this.currentSlide+1;
  this.onSlide();
  window.setTimeout(function(){
    parent.organizeSlide();
    }, 300);
}

Slider.prototype.previousSlide = function() {
  var parent = this;
  var slide = this.slides[this.currentSlide].slide;
  var nav = this.nav[this.currentSlide].nav;
  var previousSlide = this.slides[this.currentSlide == 0 ? this.slides.length-1 : this.currentSlide-1].slide;
  var previousNav = this.nav[this.currentSlide == 0 ? this.slides.length-1 : this.currentSlide-1].nav;
  var offsetLeft = parseInt(getComputedStyle(this.container.parentElement, null).width);

  transform(slide, offsetLeft);
  transform(previousSlide, 0);

  nav.style.backgroundColor = parent.backBulletColor;
  previousNav.style.backgroundColor = parent.bulletColor;

  this.currentSlide = this.currentSlide == 0 ? this.slides.length-1 : this.currentSlide-1;
  this.onSlide();
  window.setTimeout(function(){
    parent.organizeSlide();
  }, 300);
}

Slider.prototype.touchMove = function(e,o) {
  this.touch = true;
  var slide = this.slides[this.currentSlide].slide;
  var previousSlide = this.slides[this.currentSlide == 0 ? this.slides.length-1 : this.currentSlide-1].slide;
  var nextSlide = this.slides[this.currentSlide == this.slides.length-1 ? 0 : this.currentSlide+1].slide;
  var offsetLeft = o.dx,
      nextOffset = parseInt(getComputedStyle(this.container.parentElement, null).width) - offsetLeft,
      previousOffset = -parseInt(getComputedStyle(this.container.parentElement, null).width) + offsetLeft;
      stableOffset = parseInt(getComputedStyle(this.container.parentElement, null).width);
  if(!this.auto) {
    if(o.inMotion && (o.direction == "right" || o.direction == "left")) {
      e.preventDefault();
      transition(slide, "transform", 0, this.easing);
      transition(previousSlide, "transform", 0, this.easing);
      transition(nextSlide, "transform", 0, this.easing);
      // console.log(o.direction);
      if(o.direction == "right" && this.slides.length == 2) {
        transform(nextSlide, (-stableOffset+offsetLeft));
        transform(slide, offsetLeft);
      } else if (o.direction == "left" && this.slides.length == 2) {
        transform(nextSlide, (stableOffset+offsetLeft));
        transform(slide, offsetLeft);
      } else {
        transform(previousSlide, (-stableOffset+offsetLeft));
        transform(slide, offsetLeft);
        transform(nextSlide, (stableOffset+offsetLeft));
      }

    } else {
      transition(slide, "transform", this.duration, this.easing);
      transition(previousSlide, "transform", this.duration, this.easing);
      transition(nextSlide, "transform", this.duration, this.easing);
      if(offsetLeft > this.container.clientWidth/4 && o.direction == "right") {
        this.previousSlide();
      } else if (offsetLeft < -(this.container.clientWidth/4) && o.direction == "left") {
        this.nextSlide();
      } else {
        if(o.direction == "right" && this.slides.length == 2) {
          transform(nextSlide, -stableOffset);
          transform(slide, 0);
        } else if (o.direction == "left" && this.slides.length == 2) {
          transform(nextSlide, stableOffset);
          transform(slide, 0);
        } else {
          transform(previousSlide, -stableOffset);
          transform(slide, 0);
          transform(nextSlide, stableOffset);
        }
      }
    }
  }

}

Slider.prototype.on = function(element, evt, callback) {
  var parent = this;
  var isSlide = evt == 'slide',
    detecttouch = !!('ontouchstart' in window) || !!('ontouchstart' in document.documentElement) || !!window.ontouchstart || !!window.onmsgesturechange || (window.DocumentTouch && window.document instanceof window.DocumentTouch);
    if (isSlide) {
      var o = {},
        evtStarted = false,
        evtStart = function(e) {
          var evt = e.changedTouches ? e.changedTouches[0] : e;
          evtStarted = true;
          o = {
            start: {
              left: evt.pageX,
              top: evt.pageY
            }
          };
        },
        evtEnd = function(e) {
          if (!evtStarted) return;
          var evt = e.changedTouches ? e.changedTouches[0] : e;
          o.end = {
            left: evt.pageX,
            top: evt.pageY
          };
          o.dx = o.end.left - o.start.left;
          o.dy = o.end.top - o.start.top;
          o.angle = Math.atan2(o.dy, o.dx);
          o.angle *= 180 / Math.PI;
          o.inMotion = (e.type == 'touchmove' || e.type == 'mousemove');
          o.direction = Math.abs(o.dx) > Math.abs(o.dy) ? ('' + o.dx).indexOf('-') != -1 ? 'left' : 'right' : ('' + o.dy).indexOf('-') != -1 ? 'top' : 'bottom',
            callback.apply(parent, [e, o]);
          if (o.inMotion == false) evtStarted = false;
        };
      if (detecttouch) {
        element.addEventListener('touchstart', evtStart, false);
        element.addEventListener('touchmove', evtEnd, false);
        element.addEventListener('touchend', evtEnd, false);
      } else {
        element.addEventListener('mousedown', evtStart, false);
        element.addEventListener('mousemove', evtEnd, false);
        element.addEventListener('mouseup', evtEnd, false);
      }
    } else {
      element.addEventListener(evt, callback);
    }
  return this;
};

Slider.prototype.init = function() {
  var parent = this;
  this.makeSlider();
  window.addEventListener("resize", function(){parent.organizeSlide.call(parent)});
  window.addEventListener("orientationchange", function(){parent.organizeSlide.call(parent)});
}

}());
