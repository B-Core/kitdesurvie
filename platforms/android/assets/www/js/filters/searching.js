kitdesurvie.filter("searching", function(){
  return function(input, searchTerm){
    if(typeof searchTerm == "string" && searchTerm !== "") {
      var reg = new RegExp(searchTerm,"gim");
      if(searchTerm.length == 1) {
        if(reg.test(input.title[0])) {
          return true;
        }
      } else {
        searchTerm = searchTerm.split(",");
        var here = false;
        for(key in input){
          searchTerm.forEach(function(search){
            var reg = new RegExp(search,"gim");
            if(reg.test(input[key])){
              here = true;
            }
          });
        }
        return here;
      }
    } else {
      return true;
    }
  };
});
