var overlay = _("#overlay")[0];
var researchToggle = _("#researchToggle")[0];

function toggleOverlay() {
  on = typeof on !== "undefined" ? on : false;
  if(on) {
    overlay.style.display = "none";
    overlay.style.opacity = 0;
  } else {
    overlay.style.display = "block";
    overlay.style.opacity = 0.5;
  }
}

function animateWordPopUp(newOne) {
  var wordPopUp = newOne ? _("#newWordPopUpBox") : _("#wordPopUpBox");
  var submit = newOne ? _("#newWordPopUpBox input[type=submit]")[0] : _("#wordPopUpBox input[type=submit]")[0];

  wordPopUp.transition({
    duration : 0.4,
    delay: 0,
    easing: "ease-in-out",
    style: {
      opacity: 1,
      transform : "translate(-50%,-50%)",
      "webkit-transform": "translate(-50%,-50%)"
    }
  });
  function wordPopOut(){
    overlay.style.opacity = 0;
    overlay.removeEventListener("click", arguments.callee);
    submit.removeEventListener("click", arguments.callee);
    wordPopUp.transition({
      duration : 0.1,
      delay: 0,
      easing: "ease-out",
      style: {
        transform : "translate(-50%,-50%)scale(1.05)",
        "webkit-transform": "translate(-50%,-50%)scale(1.05)"
      }
    },function(){
      wordPopUp.transition({
        duration : 0.3,
        delay: 0,
        easing: "ease-in-out",
        style: {
          transform : "translate(-50%,-50%)scale(0.000001)",
          "webkit-transform": "translate(-50%,-50%)scale(0.000001)"
        }
      }, function(){
        wordPopUp.css({
          opacity:0,
          transform : "translate(-50%,-1000%)scale(1)",
          "webkit-transform": "translate(-50%,-1000%)scale(1)"
        })
        overlay.style.display = "none";
      });
    });
  };
  overlay.addEventListener("click", wordPopOut);
  submit.addEventListener("click", wordPopOut);
}

function animateMouvementPopUp(newOne) {
  var mouvementPopUp = newOne ? _("#newMouvementPopUpBox") : _("#mouvementPopUpBox");
  var submit = newOne ? _("#newMouvementPopUpBox input[type=submit]")[0] : _("#mouvementPopUpBox input[type=submit]")[0];

  mouvementPopUp.transition({
    duration : 0.4,
    delay: 0,
    easing: "ease-in-out",
    style: {
      opacity: 1,
      transform : "translate(-50%,-50%)",
      "webkit-transform": "translate(-50%,-50%)"
    }
  });
  function mouvementPopOut(){
    overlay.style.opacity = 0;
    overlay.removeEventListener("click", arguments.callee);
    submit.removeEventListener("click", arguments.callee);
    mouvementPopUp.transition({
      duration : 0.1,
      delay: 0,
      easing: "ease-out",
      style: {
        transform : "translate(-50%,-50%)scale(1.05)",
        "webkit-transform": "translate(-50%,-50%)scale(1.05)"
      }
    },function(){
      mouvementPopUp.transition({
        duration : 0.3,
        delay: 0,
        easing: "ease-in-out",
        style: {
          transform : "translate(-50%,-50%)scale(0.000001)",
          "webkit-transform": "translate(-50%,-50%)scale(0.000001)"
        }
      }, function(){
        mouvementPopUp.css({
          opacity:0,
          transform : "translate(-50%,-1000%)scale(1)",
          "webkit-transform": "translate(-50%,-1000%)scale(1)"
        })
        overlay.style.display = "none";
      });
    });
  };
  overlay.addEventListener("click", mouvementPopOut);
  submit.addEventListener("click", mouvementPopOut);
}

function animateArtistePopUp(newOne) {
  var artistePopUp = newOne ? _("#newArtistePopUpBox") : _("#artistePopUpBox");
  var submit = newOne ? _("#newArtistePopUpBox input[type=submit]")[0] : _("#artistePopUpBox input[type=submit]")[0];

  artistePopUp.transition({
    duration : 0.4,
    delay: 0,
    easing: "ease-in-out",
    style: {
      opacity: 1,
      transform : "translate(-50%,-50%)",
      "webkit-transform": "translate(-50%,-50%)"
    }
  });
  function artistePopOut(){
    overlay.style.opacity = 0;
    overlay.removeEventListener("click", arguments.callee);
    submit.removeEventListener("click", arguments.callee);
    artistePopUp.transition({
      duration : 0.1,
      delay: 0,
      easing: "ease-out",
      style: {
        transform : "translate(-50%,-50%)scale(1.05)",
        "webkit-transform": "translate(-50%,-50%)scale(1.05)"
      }
    },function(){
      artistePopUp.transition({
        duration : 0.3,
        delay: 0,
        easing: "ease-in-out",
        style: {
          transform : "translate(-50%,-50%)scale(0.000001)",
          "webkit-transform": "translate(-50%,-50%)scale(0.000001)"
        }
      }, function(){
        artistePopUp.css({
          opacity:0,
          transform : "translate(-50%,-1000%)scale(1)",
          "webkit-transform": "translate(-50%,-1000%)scale(1)"
        })
        overlay.style.display = "none";
      });
    });
  };
  overlay.addEventListener("click", artistePopOut);
  submit.addEventListener("click", artistePopOut);
}

function animateOeuvrePopUp() {
  var oeuvrePopUp =_("#newOeuvrePopUpBox");
  var submit = _("#newOeuvrePopUpBox input[type=submit]")[0];

  oeuvrePopUp.transition({
    duration : 0.4,
    delay: 0,
    easing: "ease-in-out",
    style: {
      opacity: 1,
      transform : "translate(-50%,-50%)",
      "webkit-transform": "translate(-50%,-50%)"
    }
  });
  function oeuvrePopOut(){
    submit.removeEventListener("click", arguments.callee);
    oeuvrePopUp.transition({
      duration : 0.1,
      delay: 0,
      easing: "ease-out",
      style: {
        transform : "translate(-50%,-50%)scale(1.05)",
        "webkit-transform": "translate(-50%,-50%)scale(1.05)"
      }
    },function(){
      oeuvrePopUp.transition({
        duration : 0.3,
        delay: 0,
        easing: "ease-in-out",
        style: {
          transform : "translate(-50%,-50%)scale(0.000001)",
          "webkit-transform": "translate(-50%,-50%)scale(0.000001)"
        }
      }, function(){
        oeuvrePopUp.css({
          opacity:0,
          transform : "translate(-50%,-1000%)scale(1)",
          "webkit-transform": "translate(-50%,-1000%)scale(1)"
        });
      });
    });
  };
  overlay.addEventListener("click", oeuvrePopOut);
  submit.addEventListener("click", oeuvrePopOut);
}

function animateOeuvrePopUp2() {
  var oeuvrePopUp2 =_("#newOeuvrePopUpBox2");
  var submit2 = _("#newOeuvrePopUpBox2 input[type=submit]")[0];

  oeuvrePopUp2.transition({
    duration : 0.4,
    delay: 0,
    easing: "ease-in-out",
    style: {
      opacity: 1,
      transform : "translate(-50%,-50%)",
      "webkit-transform": "translate(-50%,-50%)"
    }
  });
  function oeuvrePopOut2(){
    submit2.removeEventListener("click", arguments.callee);
    oeuvrePopUp2.transition({
      duration : 0.1,
      delay: 0,
      easing: "ease-out",
      style: {
        transform : "translate(-50%,-50%)scale(1.05)",
        "webkit-transform": "translate(-50%,-50%)scale(1.05)"
      }
    },function(){
      oeuvrePopUp2.transition({
        duration : 0.3,
        delay: 0,
        easing: "ease-in-out",
        style: {
          transform : "translate(-50%,-50%)scale(0.000001)",
          "webkit-transform": "translate(-50%,-50%)scale(0.000001)"
        }
      }, function(){
        oeuvrePopUp2.css({
          opacity:0,
          transform : "translate(-50%,-1000%)scale(1)",
          "webkit-transform": "translate(-50%,-1000%)scale(1)"
        });
      });
    });
  };
  overlay.addEventListener("click", oeuvrePopOut2);
  submit2.addEventListener("click", oeuvrePopOut2);
}

function toggleResearchBar() {
  var down = false;

  function toggleBar() {
    if(!down) {
      _(".searchbar").transition({
        style: {
          transform: "translate(0,0)",
          "-webkit-transform": "translate(0,0)"
        },
        duration : 0.4,
        delay : 0
      });
      down = true;
    } else {
      _(".searchbar").transition({
        style: {
          transform: "translate(0,-100px)",
          "-webkit-transform": "translate(0,-100px)"
        },
        duration : 0.4,
        delay : 0
      });
      down = false;
    }
  }

  researchToggle.addEventListener("click", toggleBar);
}

if( window.innerWidth < 1080 ){
  var slider = new Slider("app", {bulletColor:"rgb(165, 235, 185)", backBulletColor:"rgb(206, 233, 220)"});
  slider.init();
}
