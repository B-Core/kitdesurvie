kitdesurvie.service("kitdesurvieService",function($http, $q){

var getData = function(querie,handleSuccess, handleError) {
  var request = $http({
                      method: "get",
                      url: "http://178.62.113.16:3567/"+querie
                    })
  return( request.then( handleSuccess, handleError ) );
};
var pushData = function(querie,dataToPush, handleSuccess, handleError) {
  var request = $http({
                    method: "post",
                    url: "http://178.62.113.16:3567/"+querie,
                    data: dataToPush});
  return( request.then( handleSuccess, handleError ) );
};
var removeData = function(querie,dataId, handleSuccess, handleError) {
  var request = $http({
                    method: "delete",
                    url: "http://178.62.113.16:3567/"+querie+"/" + dataId
                  });
  return( request.then( handleSuccess, handleError ) );
};
var modifyData = function(querie,dataId, dataUpdate, handleSuccess, handleError) {
  var request = $http({
                    method: "put",
                    url: "http://178.62.113.16:3567/"+querie+"/" + dataId,
                    data: dataUpdate
                  });
  return( request.then( handleSuccess, handleError ) );
};
return {
  getData : getData,
  pushData : pushData,
  removeData : removeData,
  modifyData : modifyData
}
});
